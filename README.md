# Thesis on the theme of automated infrastructure deployment.

## Description
This set of scripts is used to deploy the infrastructure for creating a web-application in `stage` and `prod` environments.
As a result of this script, the following resources will be created in Yandex CLoud:
* `service accunt` - 1pcs (which the following roles:)
    * `compute.admin`
    * `vpc.admin`
    * `k8s.admin`
    * `kms.admin`
    * `iam.serviceAccounts.admin`
    * `resource-manager.admin`
    * `k8s.cluster-api.cluster-admin`
* `vpc` - 1 pcs
* `subnet` - 6 pcs (in the following zones:)
    * `ru-central1-a` 2 pcs (`pablic` and `private`)
    * `ru-central1-b` 2 pcs (`pablic` and `private`)
    * `ru-central1-c` 2 pcs (`pablic` and `private`)
* `gateway` - 3 pcs (in the following zones:)
    * `ru-central1-a` 1 pcs (`pablic`)
    * `ru-central1-b` 1 pcs (`pablic`)
    * `ru-central1-c` 1 pcs (`pablic`)
* `compute instance` - 4 pcs
    * `gitlab`
    * `gitlab-runner0`
    * `gitlab-runner1`
    * `gitlab-runner2`
* `k8s cluster` - 1 pcs (which 3 workplane nodes in the following zones:)
    * `ru-central1-a` 1 pcs (`private`)
    * `ru-central1-b` 1 pcs (`private`)
    * `ru-central1-c` 1 pcs (`private`)
  
## Requirements
The operability of the configuration has been tested on the `debian-11.5.0-amd64` and `linuxmint-21-cinnamon` operating systems.

1) Folder of YC cloud must be created
2) The following packages must be installed on bastion instance:
   * `yc >= 0.97.0`
   * `terraform >= 1.3.2`
3) The installed packages in the last item must be configured with the following commands:
   * `yc init`
   * `terraform login`
   * `terraform workspace select prod && terraform -chdir=terraform init`
   * `terraform workspace select stage && terraform -chdir=terraform init`
4) The user from whom the launch is performed should be able to elevate privileges using the `sudo` command without entering a password.

## Quick start
### Set the following environment variables or inter it on startup manually
* `export BASE_DIR=<PROJECT_DIR>` STR, example: `$PWD`
* `export TF_VAR_ssh_public_key=<SSH_PUBLIC_KEY>` STR, example: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC...
* `export TF_VAR_host_user=<HOST_USER>` STR, example: user
* `export TF_VAR_cloud_id=<CLOUD_ID>` STR, example: b1gsvg52168fl1f4agtc
* `export TF_VAR_folder_id=<FOLDER_ID>` STR, example: b1g82649tmplil5u4286
* `export TF_VAR_token=<YC_TOKEN_WICH_ADMIN_ROLE>` STR, example: AQAAAAAXzWNmAATuwZWOiNcH4Tc1jBZnyBYLJRc
* `export SA_NAME=<YC_SERVICE_ACCOUNT_NAME>` STR, example: user
* `export SA_ROOT_NAME=<YC_LOCAL_PROFILE_WICH_ADMINE_ROLE_NAME>` STR, example: user
* `export ENVIRONMENT=<ENVIRONMENT(stage|prod)>` STR, example: prod
* `export GRAFANA_URL_USER=<GRAFANA_URL_USER>` STR, example: user
* `export GRAFANA_URL_USER_PASSWORD=<GRAFANA_URL_USER_PASSWORD>` STR, example: user123
* `export QBEC_VERSION=<QBEC_VERSION(0.15.2)>` STR, example: 0.15.2
* `export GO_VERSION=<GO_VERSION(1.19.2)>` STR, example: 1.19.2
* `export PROJECT_NAME=<PROJECT_NAME>` STR, example: webapp
* `export PROJECT_REPOSITORY_URL=<PROJECT_REPOSITORY_URL_FOR_CLONE>` STR, example: https://gitlab.com/cryptonet/graduate-work-webapp
* `export DOMAIN=<DOMAIN>` STR, example: example.com
* `export SSL_EMAIL=<SSL_EMAIL>` STR, example: admin@example.com
* `export GITLAB_REGISTRY_PORT=<GITLAB_REGISTRY_PORT>` INT, example: 5555

### Deploying

#### 1. First, deploy the `stage` environment by running the following commands:
```shell
export ENVIRONMENT=stage
. bin/run.sh
```

#### 2. Then expand the `prod` environment by running the following commands:
```shell
export ENVIRONMENT=prod
. bin/run.sh
```

As a result of the script, information for connection will be displayed on the screen. 
You need to follow the link to the web `Gitlab-CE` interface and change the `root` password.

#### 3. Setting the settings `Gitlab-runner`
This item has not been automated yet. All the following actions in this item will have to be performed manually.

#### 3.1. Create `Poject access token` token.
In the `Gitlab-CE` web interface, create [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) which name `webapp`.
After that you need to encode the token which the [manual](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#log-in-to-docker-hub).
To make configuration file from the token run the following command:
```shell
export GITLAB_REGISTRY_TOKEN=<GITLAB_REGISTRY_TOKEN> # or enter it manually on startup
. bin/ansible/secret.sh
```

#### 3.2 Create blank project.
You need to create a [blank project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-blank-project) which a same name as an ansible `project_name` variable exist.

#### 3.3 Get `url` repository.
Get `url` of new repository on a general page after create it.

#### 3.4 Add `ssh public key`.
Add your `ssh public key` to the project. And get a `fingerprint` after that.

#### 3.5. Set protect variables.
Set the following [protected variables](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) in the project web interface of `Gitlab-CE` instance.
* `K8S_SERVER` - Type -VARIABLE
* `K8S_CERTIFICATE_AUTHORITY_DATA` Type - VARIABLE
* `K8S_USER_TOKEN` Type - VARIABLE
* `GITLAB_REGISTRY_DATA` Type - FILE (has been created in `3.1` paragraph)

#### 3.6 Set variables.
In the `Gitlab-CE` web interface, set [protected tags](https://docs.gitlab.com/ee/user/project/protected_tags.html) according to the `v*` template.

#### 3.7. Get `registration token`.
Get `registration token` in the web `Gitlab-CE` interface, as says [here](https://docs.gitlab.com/runner/register/).
And don't forget disable shared runners.

If desired, you can set the `registration token` and `repository url` as an environment variable or enter it manually when starting the playbook in step 5.

For example:
```shell
export REGISTRATION_TOKEN=<REGISTRATION_TOKEN>
export GITLAB_REPOSITORY_URL=<GITLAB_REPOSITORY_URL>
```

#### 3.8. Make `A` or `AAAA` `DNS` record.
Make `A` or `AAAA` `DNS` record for `Gitlab-CE` server and wait until the host is available by domain name.
***NOTE: Without an `SSL` certificate, `gitlab registry` will not work, see the [problem](https://gitlab.com/gitlab-org/gitlab/-/issues/20810).***

#### 4. Install `Gitlab-runner`.
```shell
. bin/ansible/runner.sh
```

#### 5. Deploy project into `stage k8s` cluster.
For demonstration purposes only, run the following command to deploy the test application. After 2 minutes, a change will occur in the project, followed by a `commit` with a `tag` and `deployment` into `stage k8s` cluster.
```shell
. bin/ansible/project.sh
```

## Further development of the project
* Add checks and error handlers in deployment scripts (`bin` directory).
* Create a new `bastion` instance for infrastructure management. Allow ssh access to all nodes only from `bastion` instance.
* Implement the collection of application logs using `vector`, as well as replace the current metric collector (`node-exporter`) with `vector`.
* Add validation when checking variable values.
* Add code scanning using `sonarqube` to the `CI` process.
* Implement the change of the default `SSH` port.

## Resources
* [Webapp code repository](https://gitlab.com/cryptonet/graduate-work-terraform)
* [Webapp image repository](https://hub.docker.com/repository/docker/cryptodeveloper/graduate-work-webapp/general)

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
