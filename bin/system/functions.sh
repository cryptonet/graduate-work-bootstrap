#!/bin/bash

# Print text in "info" style.
info() {
  printf "\r\033[00;35mINFO: %s\033[0m\n" "$1"
}

# Print text in "warning" style.
warning() {
  printf "\r\033[0;43mWARNING: %s\033[0m\n" "$1"
}

# Print text in "fail" style.
fail() {
  printf "\r\033[0;31mERROR: %s\033[0m\n" "$1"
}

# Create configuration files from templates.
template() {
  envsubst < "$BASE_DIR"/templates/"$1" | tee "$2" > /dev/null
}

variable() {
    while true; do
    value=$(eval echo "\$$1")
    if [ -z "$value" ]; then
      warning "\$$1 variable is not set."
      echo "Enter value for \$$1 variable:"
      read -r var
      eval export "$1=$var"
    else
      break
    fi
  done
}

# Separate string by symbol and get last value
sep_str_and_get_last_substr() {
  IFS="$1"
  read -r -a arr <<< "$2"
  echo "${arr[${#arr[@]}-1]}"
}

# Clone git repository
get_git() {
  if [ -d "$BASE_DIR"/"$1" ]; then
    git -C "$BASE_DIR"/"$1" pull > /dev/null
  else
    git clone "$2" "$BASE_DIR"/"$1" > /dev/null
  fi
}

# Download url resource
get_url() {
  if ! [ -e "$BASE_DIR"/"$1" ]; then
    curl -Lo "$BASE_DIR"/"$1" "$2" > /dev/null
  fi
}

export -f \
  info \
  warning \
  fail \
  template \
  get_git \
  get_url \
  sep_str_and_get_last_substr \
  variable
