#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  GO_VERSION
)

for var in "${variables[@]}"; do
  variable "$var"
done

# Install dependencies.
info 'Installing dependencies.'

sudo apt-get update > /dev/null

sudo apt-get -y install \
  ca-certificates \
  curl \
  gnupg \
  lsb-release \
  jsonnet

if [ "" = "$(which docker)" ]; then
  sudo mkdir -p /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
   echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update
  sudo apt-get -y install docker-ce docker-ce-cli containerd.io
fi

if [ "" = "$(which go)" ]; then
  get_url go"$GO_VERSION".linux-amd64.tar.gz https://go.dev/dl/go"$GO_VERSION".linux-amd64.tar.gz
  rm -rf /usr/local/go && tar -C /usr/local -xzf go"$GO_VERSION".linux-amd64.tar.gz
  export PATH=$PATH:/usr/local/go/bin
  rm -f go"$GO_VERSION".linux-amd64.tar.gz

  # shellcheck disable=SC2016
  echo 'export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin' >> "$HOME"/.profile
fi
