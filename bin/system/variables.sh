#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  SA_NAME
  SA_ROOT_NAME
  TF_VAR_cloud_id
  TF_VAR_folder_id
  TF_VAR_token
  TF_VAR_ssh_public_key
  TF_VAR_host_user
  ENVIRONMENT
  GRAFANA_URL_USER
  GRAFANA_URL_USER_PASSWORD
  QBEC_VERSION
  GO_VERSION
  PROJECT_NAME
  PROJECT_REPOSITORY_URL
  DOMAIN
  SSL_EMAIL
  GITLAB_REGISTRY_PORT
)

for var in "${variables[@]}"; do
  variable "$var"
done
