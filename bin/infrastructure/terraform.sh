#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  BASE_DIR
  ENVIRONMENT
)

for var in "${variables[@]}"; do
  variable "$var"
done

# Create YC resources.
info 'Creating YC resources.'

get_git terraform https://gitlab.com/cryptonet/graduate-work-terraform.git

terraform -chdir="$BASE_DIR"/terraform workspace select "$ENVIRONMENT"
output=$(terraform -chdir="$BASE_DIR"/terraform apply --auto-approve | tail -n 35)

result=''

# Add parsed ip address to variable.
add_ip() {
  result="$result"'export '"$1"'\n'
}

# Parse ip address from string.
parse_ip() {
  echo "$1" | tr -d '[:space:]' | tr -d \" | tr -d \,
}

# Parse ip addresses from terraform output.
get_and_cut_ip() {
  host=$(echo "$output" | head -n 8)
  output=$(echo "$output" | sed 1,8d)

  readarray -t <<< "$host"

  external_ip=$(parse_ip "${MAPFILE[2]}")
  internal_ip=$(parse_ip "${MAPFILE[2 + "$2"]}")

  eval add_ip "$1"_EXTERNAL_IP="$external_ip"
  eval add_ip "$1"_INTERNAL_IP="$internal_ip"
}

for i in $(seq 0 2); do
  get_and_cut_ip GITLAB_RUNNER"$i" 3
done

output=$(echo "$output" | sed 1,1d)
get_and_cut_ip "GITLAB0" 5

echo -e "$result" > "$BASE_DIR"/tmp/gitlab_addresses

. "$BASE_DIR"/tmp/gitlab_addresses

# Waite for nodes to start. The Yandex Cloud problem.
sleep 20
