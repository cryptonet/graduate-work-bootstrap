#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  SA_ROOT_NAME
  TF_VAR_folder_id
  SA_NAME
)

for var in "${variables[@]}"; do
  variable "$var"
done

info 'Creating YC service account. Please wait.'

# Activate local YC profile which "admin" role.
yc config profile activate "$SA_ROOT_NAME"

sa=$(yc iam service-account --folder-id "$TF_VAR_folder_id" list | grep "$SA_NAME" || test $? = 1 )

# Check for the existence of a service account. If it does not exist, it will be created.
if [ -n "$sa" ]; then
  warning "service account \"$SA_NAME\" already exist"
else
  yc iam service-account --folder-id "$TF_VAR_folder_id" create --name "$SA_NAME"
fi

# Get service account id.
sa_id=$(sep_str_and_get_last_substr " " \
 "$(yc iam service-account --folder-id "$TF_VAR_folder_id" get --name "$SA_NAME" | grep id)")

declare -a services=(
  'compute.admin'
  'vpc.admin'
  'k8s.admin'
  'kms.admin'
  'iam.serviceAccounts.admin'
  'resource-manager.admin'
  'k8s.cluster-api.cluster-admin'
)

# Add necessary roles to "$SA_NAME" service account.
for srv in "${services[@]}"; do
  yc resource-manager folder add-access-binding \
  --id "$TF_VAR_folder_id" \
  --role "$srv" \
  --subject serviceAccount:"$sa_id"
  info "\"$srv\" role hase benn added to \"$SA_NAME\" service account."
done

# Create a key for the service account.
yc iam key create \
  --service-account-name "$SA_NAME" \
  --folder-id "$TF_VAR_folder_id" \
  --output "$BASE_DIR"/tmp/"$SA_NAME"-key.json > /dev/null

ya_profile=$(yc config profile list | grep "$SA_NAME" || test $? = 1 )

# Check for the existence of a local profile which a same name as "$SA_MANE" and activate it.
# If it does not exist, it will be created.
if [ -n "$ya_profile" ]; then
  yc config profile activate "$SA_NAME"
  warning "Found an existing local profile \"$SA_NAME\""
else
  yc config profile create "$SA_NAME" > /dev/null
fi

# Add the service account key to the local profile.
yc config set service-account-key "$BASE_DIR"/tmp/"$SA_NAME"-key.json > /dev/null

rm -f "$BASE_DIR"/tmp/"$SA_NAME"-key.json

TF_VAR_token=$(yc iam create-token)
export TF_VAR_token

# The output to the screen is an "$TF_VAR_token", in case the "--print" flag is set.
if [ "$1" = '--print' ]; then
  printf "TF_VAR_token: %s\n" "$TF_VAR_token"
elif [ -n "$1" ]; then
  warning "Flag \"$1\" is not supported."
fi
