#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  BASE_DIR
  GRAFANA_URL_USER
  GRAFANA_URL_USER_PASSWORD
)

for var in "${variables[@]}"; do
  variable "$var"
done

info 'Deploying "kube-prometheus".'
# Get "kube-prometheus" binaries.
get_git kube-prometheus https://github.com/prometheus-operator/kube-prometheus.git

# Deploy "kube-prometheus".
kubectl apply --server-side -f "$BASE_DIR"/kube-prometheus/manifests/setup
kubectl wait \
	--for condition=Established \
	--all CustomResourceDefinition \
	--namespace=monitoring
kubectl apply -f "$BASE_DIR"/kube-prometheus/manifests/

# Deploy "ingress-nginx" controller.
info 'Deploying "ingress-nginx" controller.'
kubectl apply -f \
 https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.4.0/deploy/static/provider/cloud/deploy.yaml
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=120s

# Create and deploy "ingress" manifests.
info 'Creating and deploying "grafana" manifests.'

# Create grafana url secret.
GRAFANA_URL_AUTH_STR=$(htpasswd -ibn "$GRAFANA_URL_USER" "$GRAFANA_URL_USER_PASSWORD" | base64)
export GRAFANA_URL_AUTH_STR
template grafana/secret.yml.tpl "$BASE_DIR"/k8s.manifests/grafana/secret.yml

# Create configs.
grafana_image=$(kubectl get deployment grafana -n monitoring -o=jsonpath='{$.spec.template.spec.containers[:1].image}')
export GRAFANA_VERSION=${grafana_image##*:}

template grafana/deployment.yml.tpl "$BASE_DIR"/k8s.manifests/grafana/deployment.yml
template grafana/service.yml.tpl "$BASE_DIR"/k8s.manifests/grafana/service.yml
template grafana/ingress.yml.tpl "$BASE_DIR"/k8s.manifests/grafana/ingress.yml

## Apply configs.
kubectl delete -f "$BASE_DIR"/k8s.manifests/grafana/deployment.yml
kubectl apply -f "$BASE_DIR"/k8s.manifests/grafana
