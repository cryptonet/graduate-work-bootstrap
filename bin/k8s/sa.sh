#!/bin/bash

# Create sa
kubectl apply -f "$BASE_DIR/k8s.manifests/gitlab.yml"

# Get K8S_USER_TOKEN
secret=$(kubectl get secrets -n webapp | grep gitlab-sa | head -n1 | awk '{print $1;}')
token_info=$(kubectl -n webapp describe secret "$secret" | grep token:)
K8S_USER_TOKEN=$(echo "${token_info#???????}" | tr -d ' ')

K8S_USER_TOKEN=$(kubectl -n webapp describe secret "$(kubectl -n webapp get secret | \
 (grep gitlab-sa || echo "$_") | awk '{print $1}')" | grep token: | awk '{print $2}')

# Get K8S_SERVER
K8S_SERVER=$(kubectl cluster-info | grep 'Kubernetes control plane' | awk '{print $NF}' | \
 sed -E 's/\x1b\[[0-9]*;?[0-9]+m//g' | cut -d '=' -f 2 | head --bytes -1)
K8S_SERVER=$(iconv -f utf8 -t ascii <<< "$K8S_SERVER")

# Get K8S_CERTIFICATE_AUTHORITY_DATA
clusters=$(cat ~/.kube/config)
line_number=$(echo "$clusters" | grep -n "$K8S_SERVER" | head -n1 | awk '{print $1;}' | tr -d ':')
index=$(("$line_number" - 1))
K8S_CERTIFICATE_AUTHORITY_DATA=$(sed "$index!d" <<< "$clusters" | sed -e 's/certificate-authority-data: //g' | tr -d ' ')

echo "K8S_CERTIFICATE_AUTHORITY_DATA $K8S_CERTIFICATE_AUTHORITY_DATA"
echo "K8S_SERVER $K8S_SERVER"
echo "K8S_USER_TOKEN $K8S_USER_TOKEN"

export K8S_USER_TOKEN K8S_SERVER K8S_CERTIFICATE_AUTHORITY_DATA
