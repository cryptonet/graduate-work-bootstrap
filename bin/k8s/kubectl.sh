#!/bin/bash

# Check for the definition of variables and, if necessary, set values.
. bin/system/functions.sh

variables=(
  BASE_DIR
  ENVIRONMENT
  TF_VAR_folder_id
)

for var in "${variables[@]}"; do
  variable "$var"
done

# Download "kubectl" binaries.
get_url kubectl https://storage.googleapis.com/kubernetes-release/release/"$(curl \
 -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)"/bin/linux/amd64/kubectl
chmod +x "$BASE_DIR"/kubectl
sudo mv "$BASE_DIR"/kubectl /usr/local/bin/kubectl

mkdir -p "$HOME"/.kube

# Configure "kubectl".
info 'Configuring "kubectl"'
yc managed-kubernetes cluster get-credentials main-"$ENVIRONMENT" \
 --folder-id "$TF_VAR_folder_id" \
 --external \
 --force \
 --kubeconfig "$HOME"/.kube/config

kubectl config set-context yc-main-"$ENVIRONMENT"
