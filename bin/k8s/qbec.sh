#!/bin/bash

. bin/system/functions.sh

# Check for the definition of variables and, if necessary, set values.
variables=(
  BASE_DIR
  QBEC_VERSION
  ENVIRONMENT
)

for var in "${variables[@]}"; do
  variable "$var"
done

get_git qbec.manifests https://gitlab.com/cryptonet/graduate-work-qbec.git

# Configuration "qbec".
info "Deploying web application."

if ! [ -d "$BASE_DIR"/qbec ]; then
  mkdir -p "$BASE_DIR"/qbec
  get_url tmp/qbec-linux-amd64.tar.gz https://github.com/splunk/qbec/releases/download/v"$QBEC_VERSION"/qbec-linux-amd64.tar.gz
  tar -C "$BASE_DIR"/qbec -xvf "$BASE_DIR"/tmp/qbec-linux-amd64.tar.gz
  rm -f "$BASE_DIR"/tmp/qbec-linux-amd64.tar.gz
fi

if [ "$ENVIRONMENT" == prod ]; then
  export WEBAPP_REPLICAS_COUNT=6
else
  export WEBAPP_REPLICAS_COUNT=3
fi

CP0_EXTERNAL_IP=$(sep_str_and_get_last_substr ' ' "$(yc --folder-id "$TF_VAR_folder_id" \
 managed-kubernetes cluster get main-"$ENVIRONMENT" | grep external_v4_address)")

sed -i "s|      replicas.*|      replicas: $WEBAPP_REPLICAS_COUNT,|g" \
 "$BASE_DIR"/qbec.manifests/environments/webapp.libsonnet
sed -i "s|      server.*|      server: https://$CP0_EXTERNAL_IP|g" "$BASE_DIR"/qbec.manifests/qbec.yaml

# Deploy web application.
"$BASE_DIR"/qbec/qbec --root "$BASE_DIR"/qbec.manifests apply webapp --yes --wait
