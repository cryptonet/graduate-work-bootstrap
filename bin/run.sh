#!/bin/bash

set -e

# Definition "$BASE_DIR" variable.
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
BASE_DIR=$(cd -- "$script_dir/.."&> /dev/null && pwd)
export BASE_DIR

# Define environment variables.
# NOTE: "setenv.sh" file will not get into the repository.
# Read "README.md" to understand which variables need to be set.
if [ -f "$BASE_DIR"/bin/setenv.sh ]; then
  . "$BASE_DIR"/bin/setenv.sh
fi

# Define common functions.
. "$BASE_DIR"/bin/system/functions.sh

# Install dependencies.
. "$BASE_DIR"/bin/system/dependencies.sh

# Check for the definition of variables and, if necessary, setting values.
. "$BASE_DIR"/bin/system/variables.sh

# Create service account and local profile YC.
. "$BASE_DIR"/bin/infrastructure/yc-sa.sh

# Create YC resources.
. "$BASE_DIR"/bin/infrastructure/terraform.sh

# Install "kubectl".
. "$BASE_DIR"/bin/k8s/kubectl.sh

# Deploy "prometheus" monitoring system.
. "$BASE_DIR"/bin/k8s/kube-prometheus.sh

if [ "$ENVIRONMENT" = 'stage' ]; then
  # Create "gitlab" service account.
  . "$BASE_DIR"/bin/k8s/sa.sh

  # Deploy web application.
  . "$BASE_DIR"/bin/k8s/qbec.sh

elif [ "$ENVIRONMENT" = 'prod' ]; then
  # Configure "Gitlab" instances.
  . "$BASE_DIR"/bin/ansible/gitlab.sh
fi
