---
kind: Service
apiVersion: v1
metadata:
  labels:
    app.kubernetes.io/component: grafana
    app.kubernetes.io/name: grafana
    app.kubernetes.io/part-of: kube-prometheus
    app.kubernetes.io/version: ${GRAFANA_VERSION}
  name: grafana
  namespace: monitoring
spec:
  selector:
    app.kubernetes.io/component: grafana
    app.kubernetes.io/name: grafana
    app.kubernetes.io/version: ${GRAFANA_VERSION}
  ports:
  - name: http
    port: 80
    targetPort: http
