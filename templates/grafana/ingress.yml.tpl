---
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  annotations:
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: monitoring
  namespace: monitoring
spec:
  ingressClassName: nginx
  rules:
  - host: grafana.netology.${DOMAIN}
    http:
      paths:
      - backend:
          service:
            name: grafana
            port:
              name: http
        path: /
        pathType: Prefix
