---
kind: Secret
apiVersion: v1
metadata:
  name: basic-auth
  namespace: monitoring
type: Opaque
data:
  auth: ${GRAFANA_URL_AUTH_STR}
