# Дипломный практикум в `Yandex.Cloud`. Александр Хайкин.

## Описание
Следующий список репозиториев предназначен для развертывания инфраструктуры разработки и сопровождения веб приложения в `stage` и `prod` окружениях, размещенных в `YC`.
* [terraform](https://gitlab.com/cryptonet/graduate-work-terraform.git) - создание `YC` ресурсов.
* [ansible](https://gitlab.com/cryptonet/graduate-work-ansible.git) - установка и настройка `Gitlab-CE` и `Gitlab-runners`.
* [webapp](https://gitlab.com/cryptonet/graduate-work-webapp) - тестовое веб приложение. **Служит только для демонстрации, в результате работы [graduate-work-bootstrap](https://gitlab.com/cryptonet/graduate-work-bootstrap.git) (описание ниже), будет склонирован на созданную инфраструктуру**.
* [webapp-image](https://hub.docker.com/r/cryptodeveloper/graduate-work-webapp) - `image` тестового веб приложения. **Служит только для демонстрации, в результате работы [graduate-work-bootstrap](https://gitlab.com/cryptonet/graduate-work-bootstrap.git) (описание ниже), будет склонирован на созданную инфраструктуру**.
* [qbec](https://gitlab.com/cryptonet/graduate-work-qbec.git) - создание `k8s` объектов.

В результате будет создана инфраструктура в `stage` и `prod` окружениях. Кластера `k8s` будут развернуты в каждом из окружений.
`Gitlab-CE` и `Gitlab-runnes` будут развернуты только в `prod` окружение. `Workplane nodes` будут расположены в приватных подсетях.
При любом `commit` будет собран `image` и отправлен в `Gitla registry`. При любом `commit` с `tag` будет собран `image` с данным `tag`, отправлен в `Gitlab registry` и развернут в `k8s` кластере с `stage` окружением.

## Демонстрация
### `Terraform Cloud`
[![terraform-cloud.png](https://i.postimg.cc/XqwH817g/terraform-cloud.png)](https://postimg.cc/m1gyZw9c)

### `Grafana`
[grafana](http://grafana.netology.stage.asicminer8.ru)
* `url nginx` пользователь: observer
* `url nginx` пароль: observer123
* `grafana` пользователь: admin
* `grafana` пароль: admin123

[![grafana.png](https://i.postimg.cc/CLzrR7XN/grafana.png)](https://postimg.cc/BXfByTqL)

### `Webapp`
[webapp](http://webapp.netology.stage.asicminer8.ru)

[![webapp.png](https://i.postimg.cc/jqBVY9rG/webapp.png)](https://postimg.cc/Jt3Ybp0q)

## Быстрый старт
Вы можете воспользоваться `README.md` файлами в каждом репозитории или использовать проект [graduate-work-bootstrap](https://gitlab.com/cryptonet/graduate-work-bootstrap.git) для упрощенного развертывания данной инфраструктуры.

P.S. Несколько моментов:
1. Вычислительные мощности инфраструктуры распределены не совсем верно, а именно:
   * Объём сетевых дисков ниже рекомендуемого т.к. увеличению препятствует квота, установленная провайдером (решаемо при запросе в техподдержку).
   * Только в целях демонстрации, мощности для `prod` окружения увеличены. Т.к. приложение не является высоконагруженным и требовательным к ресурсам, мощностей определенных для `stage` окружения будет вполне достаточно.
2. Вопреки написанному [скрипту автоматизации](https://gitlab.com/cryptonet/graduate-work-bootstrap.git) и описанной в нем документации пришлось изменить назначение развертывания в `k8s` кластер в `prod` окружение. Причиной чему стало превышение квоты на создание ресурсов `YC`, вследствие чего в настоящее время инфраструктура развернута только на `prod` окружение. 
